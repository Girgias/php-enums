# Basic PHP enums
A simple base for Enums in PHP

## Installing

```shell
composer require girgias/php-enums
```

## Features

Extend the ``Base`` class and define constants within the class.

Names and values of these constants can be verified with:

* ``isValidName('NAME')``
* ``isValidValue($mixedValue)``

## Contributing

If you'd like to contribute, please fork the repository and use a feature
branch. Pull requests are warmly welcome.

## Links

- Repository: https://gitlab.com/Girgias/php-enums
- Issue tracker: https://gitlab.com/Girgias/php-enums/issues

## Licensing

The code in this project is licensed under MIT license.
